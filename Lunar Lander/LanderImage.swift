//
//  LanderImage.swift
//  Lunar Lander
//
//  Created by student on 4/4/19.
//  Copyright © 2019 Northwest. All rights reserved.
//

import Foundation
import UIKit

class LanderImage{
    
    static var shared = LanderImage()
    private var landerImage:UIImage
    private convenience init(){
        self.init(landerImage:UIImage(named : "lander_apollo_11")!)//setting a default lander
    }//end of convenience initializer
    
    private init(landerImage:UIImage){
        self.landerImage = landerImage
    }//end of initializer
    
    func setLanderImage(landerImage:UIImage){
        self.landerImage = landerImage
    }//end of setter method
    
    func getLanderImage() -> UIImage{
        return landerImage
    }//end of getter method
    
}//end of class LanderImage
