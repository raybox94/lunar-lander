//
//  SurfaceImage.swift
//  Lunar Lander
//
//  Created by Student on 4/4/19.
//  Copyright © 2019 Northwest. All rights reserved.
//

import Foundation
import UIKit

class SurfaceImage{
    
    static var shared = SurfaceImage()
    private var surfaceImage:UIImage
    private convenience init(){
        self.init(surfaceImage:UIImage(named : "moon")!)//setting a default surface
    }//end of convenience initializer
    
    private init(surfaceImage:UIImage){
        self.surfaceImage = surfaceImage
    }//end of initializer
    
    func setSurfaceImage(surfaceImage:UIImage){
        self.surfaceImage = surfaceImage
    }//end of setter method
    
    func getSurfaceImage() -> UIImage{
        return surfaceImage
    }//end of getter method
    
}//end of class LanderImage
