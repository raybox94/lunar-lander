//
//  PlaySimulatorViewController.swift
//  Lunar Lander
//
//  Created by student on 3/11/19.
//  Copyright © 2019 Northwest. All rights reserved.
//

import UIKit
import AVFoundation

class PlaySimulatorViewController: UIViewController {
    
    var timer = Timer()
    var i:CGFloat = -12
    var location = CGPoint(x: 0, y: 0)
    var audioPlayer: AVAudioPlayer?
    
    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var fireCraft: UIImageView!
    @IBOutlet weak var surfaceImage: UIImageView!
    @IBOutlet weak var landerImage: UIImageView!
    @IBOutlet weak var surface: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        landerImage.image = LanderImage.shared.getLanderImage()
        surfaceImage.image = SurfaceImage.shared.getSurfaceImage()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(PlaySimulatorViewController.landerCalculations),userInfo: nil, repeats: true)
        // Do any additional setup after loading the view.
        background.loadGif(name: "background")
        fireCraft.isHidden = true
    }//end of viewDidLoad method
    
    @IBOutlet weak var timeLBL: UILabel!
    @IBOutlet weak var velocityLBL: UILabel!
    @IBOutlet weak var heightLBL: UILabel!
    @IBOutlet weak var fuelLBL: UILabel!
    @IBOutlet weak var thrustBTN: UIButton!
    
    @IBAction func thrustBTN(_ sender: Any) {
        print("Button Pressed")
        var thrust:Double = 0.0
        if Calculations.shared.fuel <= 0.0{
            Calculations.shared.thrustValue(thrust: 0.0)
        }else{
            thrust = 100.0
            Calculations.shared.thrustValue(thrust: thrust)
            fireCraft.isHidden = false
            print(SoundSettings.shared.getSoundAction())
            if SoundSettings.shared.getSoundAction(){
                playSaveSound()
            }//end of if statement for checking sound action
        }//end of if-else statement
    }//end of thrustBTN
    
    @objc func landerCalculations(){
        if Calculations.shared.fuel <= 0.0{
            fireCraft.isHidden = true
        }
        if thrustBTN.isTouchInside == false{
            print("Button Released")
            fireCraft.isHidden = true
            
            Calculations.shared.thrustValue(thrust: 0)
        }//end button relesed
        
        let data = Calculations.shared.craft()
        timeLBL.text = String(format:"%.2f s",data[0])
        heightLBL.text = String(format:"%.2f m",Calculations.shared.height)
        velocityLBL.text = String(format:"%.2f m/s",data[2])
        fuelLBL.text = String(format:"%.2f kg",data[3])
        if Calculations.shared.touchdownGetter(){
            self.performSegue(withIdentifier: "playViewSegue", sender: self)
            timer.invalidate()
        }//end of if statement
        if Calculations.shared.height < 1000 && Calculations.shared.height > 900{
            i = -50
            print(i)
            self.surface.transform = CGAffineTransform( translationX: 0.0, y: i)
        }
        if Calculations.shared.height < 900 && Calculations.shared.height > 800{
            i = -100
            print(i)
            self.surface.transform = CGAffineTransform( translationX: 0.0, y: i)
        }
        if Calculations.shared.height < 800 && Calculations.shared.height > 700{
            i = -150
            print(i)
            self.surface.transform = CGAffineTransform( translationX: 0.0, y: i)
        }
        if Calculations.shared.height < 700 && Calculations.shared.height > 600{
            i = -200
            print(i)
            self.surface.transform = CGAffineTransform( translationX: 0.0, y: i)
        }
        if Calculations.shared.height < 600 && Calculations.shared.height > 500{
            i = -250
            print(i)
            self.surface.transform = CGAffineTransform( translationX: 0.0, y: i)
        }
        if Calculations.shared.height < 500 && Calculations.shared.height > 400{
            i = -300
            print(i)
            self.surface.transform = CGAffineTransform( translationX: 0.0, y: i)
        }
        if Calculations.shared.height < 400 && Calculations.shared.height > 300{
            i = -350
            print(i)
            self.surface.transform = CGAffineTransform( translationX: 0.0, y: i)
        }
        if Calculations.shared.height < 300 && Calculations.shared.height > 200{
            i = -400
            print(i)
            self.surface.transform = CGAffineTransform( translationX: 0.0, y: i)
        }
        if Calculations.shared.height < 200 && Calculations.shared.height > 150{
            i = -450
            print(i)
            self.surface.transform = CGAffineTransform( translationX: 0.0, y: i)
        }
        if Calculations.shared.height < 150{
            i = -495
            print(i)
            self.surface.transform = CGAffineTransform( translationX: 0.0, y: i)
        }
        //above are the the height checking conditions and moving surface according to it
    }//end of yourTask method
    
    
    func playSaveSound(){
        let path = Bundle.main.path(forResource: "Grenade-SoundBible.com-2124844747.wav", ofType: nil)!
        let url = URL(fileURLWithPath: path)
        do {
            //create your audioPlayer in your parent class as a property
            audioPlayer = try AVAudioPlayer(contentsOf: url)
            audioPlayer?.play()
        } catch {
            print("couldn't load the file")
        }//end of exception handling
    }//end of method for play sound
    
    func showToast(message : String) {
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/4 - 75, y: self.view.frame.size.height-440, width: 350, height: 120))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .left;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.numberOfLines = 3
        toastLabel.alpha = 1.0
        toastLabel.textAlignment = NSTextAlignment.justified
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 6.0, delay: 1.0, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }//end of show toast method
    
    override func viewWillAppear(_ animated: Bool) {
        showToast(message: "To land perfectly make sure the velocity is close to zero. Long press the thrust button to burn fuel.")
    }//end of view will appear method
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}//end of class
