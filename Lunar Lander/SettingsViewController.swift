//
//  ThirdViewController.swift
//  Lunar Lander
//
//  Created by student on 2/25/19.
//  Copyright © 2019 Northwest. All rights reserved.
//

import UIKit
import MediaPlayer
import AVFoundation

class SettingsViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource {
    
    var audioPlayer = AVAudioPlayer()
    let surface = ["Moon","Mars","Jupiter","Saturn"]
    let surfaceGravity:[String:Double] = ["Moon":1.63,"Mars":3.711,"Jupiter":24.79,"Saturn":10.44]
    var surface1:String?
    @IBOutlet weak var surfacePV: UIPickerView!
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }//end of method
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return surface.count
    }//end of method
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        surface1 = surface[row]
        var gravity:Double = 0.0
        for i in surfaceGravity{
            if i.key == surface1{
                gravity = i.value
                SurfaceImage.shared.setSurfaceImage(surfaceImage: UIImage(named: i.key.lowercased())!)
            }//end of if statement
            if gravity != 0.0{
                Calculations.shared.gravity = gravity
            }//end of if statement
        }//end of for loop
    }//end of method
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return surface[row]
    }
    
    @IBAction func soundSwitch(_ sender: UISwitch) {
        if(sender.isOn == true){
            SoundSettings.shared.setSoundAction(soundAction: true)
        }else{
            SoundSettings.shared.setSoundAction(soundAction: false)
        }//end of if-else statement
    }//end of soundSwitch Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }//end of method
    
    @IBAction func aboutBTN(_ sender: Any) {
        self.performSegue(withIdentifier: "settingsViewSegue", sender: self)
    }//end of method
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}//end of class
