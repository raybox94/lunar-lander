//
//  SoundSettings.swift
//  Lunar Lander
//
//  Created by Student on 4/18/19.
//  Copyright © 2019 Northwest. All rights reserved.
//

import Foundation

class SoundSettings{
    
    static let shared = SoundSettings()
    private var soundAction:Bool
    
    private init(soundAction:Bool){
        self.soundAction = soundAction
    }//end of designated initializer
    
    private convenience init(){
        self.init(soundAction:true)
    }//end of convenience initializer
    
    func getSoundAction() -> Bool{
        return soundAction
    }//end of getter method
    
    func setSoundAction(soundAction:Bool){
        self.soundAction = soundAction
    }//end of setter method
       
}//end of class
