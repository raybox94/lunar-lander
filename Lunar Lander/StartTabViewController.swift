//
//  FirstViewController.swift
//  Lunar Lander
//
//  Created by rayaan on 23/02/19.
//  Copyright © 2019 Northwest. All rights reserved.
//

import UIKit

class StartTabViewController: UIViewController {
    
    @IBOutlet weak var landerImage: UIImageView!
    @IBOutlet weak var surfaceImage: UIImageView!
    @IBAction func playButton(_ sender: Any) {
        //need to redirict to another storyboard to start playing
        self.performSegue(withIdentifier: "startViewSegue", sender: self)
    }//end of method
    
    override func viewDidLoad() {
        super.viewDidLoad()        
        // Do any additional setup after loading the view, typically from a nib.
    }//end of viewDidLoad method
    
    override func viewWillAppear(_ animated: Bool) {
        landerImage.image = LanderImage.shared.getLanderImage()//to set lander image
        surfaceImage.image = SurfaceImage.shared.getSurfaceImage()//to set surface image
    }    
}//end of class

