//
//  Calculations.swift
//  Lunar Lander
//
//  Created by rayaan on 11/03/19.
//  Copyright © 2019 Northwest. All rights reserved.
//

import Foundation
class Calculations{
    
    static var shared = Calculations()
    
    private var touchdown:Bool
    private var fuelOut:Bool
    private var timeToTouchDown:Double
    private var timeToFuelOut:Double
    private var thrust:Double
    private var accelaration:Double
    
    // end of Variables needed during execution
    
    private convenience init(){
        self.init(touchdown: false, fuel_out: false, time_to_touch_down: 0, time_to_fuel_out: 0, thrust: 0, accelaration: 0)
    }
    
    private init(touchdown:Bool, fuel_out:Bool, time_to_touch_down:Double, time_to_fuel_out:Double, thrust:Double, accelaration:Double){
        self.touchdown = touchdown
        self.fuelOut = fuel_out
        self.timeToTouchDown = time_to_touch_down
        self.timeToFuelOut = time_to_fuel_out
        self.thrust = thrust
        self.accelaration = accelaration
    }
    // end of initializers
    
    var gravity:Double = 1.63 // Gravity on moon it changes through code when picker view item is selected
    var height:Double = 1750 // Initial height of craft
    var velocity:Double = 16.0 // Initial velocity of the craft
    var fuel:Double = 450.0 // Initial fuel in the craft
    var time:Double = 0 // end of setting initial configurations at time t = 0
    var touchdownVelocity = 0.0
    var temporaryThrust = 0.0
    
    func craft() -> [Double] {
        var result:[Double] = []
        let maxThrust:Double = 3.0
        let maxBurnRate:Double = 10.0
        let timeSlice:Double = 1.0
        let infinity:Double = 1E10
        let small:Double = 0.001 // acceleration below this will be called zero
        //end of constants
        
        result.append(time)
        result.append(height)
        result.append(velocity)
        result.append(fuel)
        var validResponse = false
        
        while !validResponse{
            if fuel <= 0.0{
                thrust = 0.0
            }else{
                thrust = temporaryThrust
            }//end of  if-else statement
            validResponse = true
        }//end of while loop
        print()//method call
        // Calculate the acceleration, and how long fuel will last
        accelaration = (gravity - maxThrust*thrust/100.0)
        timeToFuelOut = fuel/(maxBurnRate*thrust/100.0)
        // calculate time_to_touchdown assuming fuel never runs out
        let discrim:Double = velocity * velocity + 2 * height * accelaration
        if discrim < 0{
            timeToTouchDown = infinity
        }else if(abs(accelaration) < small){
            timeToTouchDown = height/velocity
        }else{
            timeToTouchDown = (velocity-sqrt(discrim))/(-accelaration)
        }
        // Handle the case when fuel will run out first
        touchdown = fuelOut || timeToTouchDown<timeSlice;
        // If not, advance everything one time period and keep going
        if !touchdown{
            time = time + timeSlice
            fuel = fuel - timeSlice*maxBurnRate*thrust/100.0
            height = height - timeSlice*velocity - accelaration*timeSlice*timeSlice/2
            velocity = velocity + timeSlice*accelaration
        }//end of if not touch down
        print("Touchdown at time \(time+timeToTouchDown)")
        touchdownVelocity = velocity+accelaration*timeToTouchDown
        print("Velocity at touchdown: \(touchdownVelocity)")
        print(height)
        print(gravity)
        //We have kept print statement in almost every method for debugging to check those methods are been called or  not
        return result
    }//end of function craft
    
    func result() -> String{
        var result:String = ""
        if touchdownVelocity <= 0.5{
            result = "Perfect Landing"
        }else if touchdownVelocity <= 1.0{
            result = "Soft Landing"
        }else if touchdownVelocity <= 2.0{
            result = "Good Landing"
        }else if touchdownVelocity <= 5.0{
            result = ["Hard Landing", "Ouch"][Int.random(in:0..<2)]
        }else if touchdownVelocity <= 10.0{
            result = "Lander Damaged"
        }else if touchdownVelocity <= 20.0{
            result = ["No Survivor", "Better luck next time!!"][Int.random(in:0..<2)]
        }else{
            result = ["Nothing left of the craft", "You can do  better than this!!!!!"][Int.random(in:0..<2)]
        }
        return result
    }//end of function result
    
    
    func thrustValue(thrust:Double){
        self.temporaryThrust = thrust
    }//end of setter method for thrust
    
    func touchdownGetter() -> Bool{
        return touchdown
    }//end of method
    
}//end of class

