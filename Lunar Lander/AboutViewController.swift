//
//  AboutViewController.swift
//  Lunar Lander
//
//  Created by student on 3/11/19.
//  Copyright © 2019 Northwest. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }//end of viewDidLoad method
    
    @IBAction func startBTN(_ sender: Any) {
        // self.presentingViewController?.dismiss(animated: true, completion:nil)
        self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
    }//end of method
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
